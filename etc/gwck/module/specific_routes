#!/bin/sh
#
# gwck specific_routes module
#
# Copyright (C) 2008-2012 Simó Albert i Beltran
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Version 0.1.20111008.1


#############
# Variables #
#############

# Route for hosts tests.
# Empty for autodetect.
[ -z "$GWCK_MOD_SPECIFIC_ROUTES_ROUTE" ] \
&& GWCK_MOD_SPECIFIC_ROUTES_ROUTE=""

# Routing table.
[ -z "$GWCK_MOD_SPECIFIC_ROUTES_RT_TABLE" ] \
&& GWCK_MOD_SPECIFIC_ROUTES_RT_TABLE="200"

# The filter to select the route.
[ -z "$GWCK_MOD_SPECIFIC_ROUTES_FILTER_SELECT_ROUTE" ] \
&& GWCK_MOD_SPECIFIC_ROUTES_FILTER_SELECT_ROUTE="^default\b"

# The filter to ignore routes.
[ -z "$GWCK_MOD_SPECIFIC_ROUTES_FILTER_IGNORE_ROUTES" ] \
&& GWCK_MOD_SPECIFIC_ROUTES_FILTER_IGNORE_ROUTES="\bdev \(bmx6_out\|x6\)"

#############
# Functions #
#############

gwck_mod_specific_routes_get_route()
{
	if [ -z "$GWCK_MOD_SPECIFIC_ROUTES_ROUTE" ]
	then
		# Get gateway from default route in main table.
		GWCK_MOD_SPECIFIC_ROUTES_ROUTE="$(ip -$1 route | grep -v "$GWCK_MOD_SPECIFIC_ROUTES_FILTER_IGNORE_ROUTES" | grep -m1 "$GWCK_MOD_SPECIFIC_ROUTES_FILTER_SELECT_ROUTE" | sed -e "s/\bexpires [^ ]\+\b//g")"
	fi
}

##
# Test IP address.
#
gwck_mod_specific_routes_check_ip()
{
	if echo "$*" | grep -q ":"
	then
		ipv6calc 2> /dev/null
		if [ $? != 127 ]
		then
			if ipv6calc -q "$*" > /dev/null 2> /dev/null
			then
				return 0
			else
				return 1
			fi
		else
			log "WARNING: specific_routes module needs the ipv6calc program to check IPv6 addresses. Please install it."
			return 0
		fi
	else
		if echo "$*" | grep -E -q '\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b'
		then
			return 0
		else
			return 1
		fi
	fi
}

##
# Create and delete specific routes for hosts test.
# usage:
#  gwck_mod_specific_routes_specific_routes add
#  gwck_mod_specific_routes_specific_routes del
#
gwck_mod_specific_routes_specific_routes()
{
	local action=$1
	
	if echo "$HOSTS_TEST" | grep -q ":"
	then
		local ipv=6
	else
		local ipv=4
	fi
	
	# Define route to GWCK_MOD_SPECIFIC_ROUTES_ROUTE (global variable).
	# Gateway IP address.
	test "$action" = "add" && gwck_mod_specific_routes_get_route $ipv
	# Check if default route are configured.
	if [ -n "$GWCK_MOD_SPECIFIC_ROUTES_ROUTE" ]
	then
		# For each test host
		for i in $HOSTS_TEST
		do
			# Check if test host IP address is correct.
			if gwck_mod_specific_routes_check_ip $i
			then
				# Check if a previous rule exists to test a host.
				if test "$action" = "add"
				then
					if ip -"$ipv" rule show | grep -q "from all to $i lookup ${GWCK_MOD_SPECIFIC_ROUTES_RT_TABLE} $"
					
					then
						# Save previous rule exists state for test a host.
						eval GWCK_MOD_SPECIFIC_ROUTES_RULE_$(echo $i | sed -e "s/[\.:]/_/g")="true"
					else
						# Save previous rule don't exists state for test a host.
						eval GWCK_MOD_SPECIFIC_ROUTES_RULE_$(echo $i | sed -e "s/[\.:]/_/g")="false"
					fi
				fi
				# If previous rule for test host don't exists create or delete rule.
				if ! "$(eval echo \$GWCK_MOD_SPECIFIC_ROUTES_RULE_$(echo $i | sed -e "s/[\.:]/_/g"))"
				then
					# Create or delete rule for test a host.
					ip -"$ipv" rule $action to $i table $GWCK_MOD_SPECIFIC_ROUTES_RT_TABLE 2>&1 | log \
					&& log "$action rule for $i to table $GWCK_MOD_SPECIFIC_ROUTES_RT_TABLE"
				else
					log "not $action rule for $i to table $GWCK_MOD_SPECIFIC_ROUTES_RT_TABLE"
				fi
			fi
		done
		# Check if a previous route exists.
		if test "$action" = "add"
		then
			if ip -"$ipv" route show table $GWCK_MOD_SPECIFIC_ROUTES_RT_TABLE | grep -q "^$GWCK_MOD_SPECIFIC_ROUTES_ROUTE"
			then
				# Save previous route exists state.
				GWCK_MOD_SPECIFIC_ROUTES_ROUTE_EXISTS="true"
			else
				# Save previous route don't exists state.
				GWCK_MOD_SPECIFIC_ROUTES_ROUTE_EXISTS="false"
			fi
		fi
		# If previous route don't exists create or delete route for tests.
		if ! "$GWCK_MOD_SPECIFIC_ROUTES_ROUTE_EXISTS"
		then
			# Create or delete route for tests.
			ip -"$ipv" route $action $GWCK_MOD_SPECIFIC_ROUTES_ROUTE table $GWCK_MOD_SPECIFIC_ROUTES_RT_TABLE 2>&1 | log \
			&& ip -"$ipv" route flush cache 2>&1 | log \
			&& log "$action route $GWCK_MOD_SPECIFIC_ROUTES_ROUTE in table $GWCK_MOD_SPECIFIC_ROUTES_RT_TABLE"
		else
			log "not $action route $GWCK_MOD_SPECIFIC_ROUTES_ROUTE in table $GWCK_MOD_SPECIFIC_ROUTES_RT_TABLE"
		fi
	fi
}

##############
# gwck hooks #
##############

gwck_mod_specific_routes_init()
{
	gwck_mod_specific_routes_specific_routes add
}

#gwck_mod_specific_routes_connect()
#{
#	:
#}

#gwck_mod_specific_routes_disconnect()
#{
#	:
#}

gwck_mod_specific_routes_term()
{
	gwck_mod_specific_routes_specific_routes del
}

#############
# Main code #
#############

# Actions in run-time load module.
:
